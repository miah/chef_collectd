require 'spec_helper'

describe 'collectd::_install_from_package' do
  let(:chef_run) { ChefSpec::Runner.new.converge(described_recipe) }

  it 'runs a execute kill collectdmon' do
    expect(chef_run).to_not run_execute('kill_collectdmon')
  end

  it 'installs the collectd package' do
    expect(chef_run).to install_package('collectd')
  end
end
