require 'spec_helper'

describe 'collectd::default' do
  let(:chef_run) { ChefSpec::Runner.new.converge(described_recipe) }

  it 'installs from package' do
    expect(chef_run).to include_recipe('collectd::_install_from_package')
  end
end
