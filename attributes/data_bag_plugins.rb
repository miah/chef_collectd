collectd_default_data_bag_attributes = {
  data_bag_name: 'collectd',
  plugins: []
}

node.default[:collectd].merge!(collectd_default_data_bag_attributes)
